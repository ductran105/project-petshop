"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://pucci-mart.onrender.com/api";

// Tổng số bản ghi của hệ thống
let gTotalRecords;

// Tổng số trang.
let gTotalPages;

// Tham số limit (Số lượng bản ghi tối đa trên 1 trang)
const gPerPage = 8;

const gQueryParams = new URLSearchParams({
  '_limit': gPerPage
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

$(document).ready(onPageLoading);


/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoading() {
  makeCall(1);
}


/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function createPagination(vPageNum) {
  $("#page_container").html("");
  
  if (vPageNum==1){
    $("#page_container").append("<li class='page-item disabled'><a class='page-link'><img src='./img/prev.svg' alt=''></a></li>");
  } else if (vPageNum == gTotalPages) {
    $("#page_container").append("<li class='page-item active' onclick='makeCall(" + (vPageNum - 1) + ")'><a href='javascript:void(0)' class='page-link'><img src='./img/prev.svg' alt=''></a></li>");
  } else {
    $("#page_container").append("<li class='page-item' onclick='makeCall(" + (vPageNum - 1) + ")'><a href='javascript:void(0)' class='page-link'><img src='./img/prev.svg' alt=''></a></li>");
  }

  if (vPageNum == gTotalPages) {
    $("#page_container").append("<li class='page-item disabled'><a href='javascript:void(0)' class='page-link'><img src='./img/next.svg' alt=''></a></li>");
  } else {
    $("#page_container").append("<li class='page-item active' onclick='makeCall(" + (vPageNum + 1) + ")'><a href='javascript:void(0)' class='page-link'><img src='./img/next.svg' alt=''></a></li>");
  }
}

function fetch_data(vPageNum) {
  gQueryParams.set('_page', vPageNum - 1);

  showSpinner();
  $.ajax({
    type: 'get',
    url: gBASE_URL + '/pets?' + gQueryParams.toString(),
    dataType: 'json',
    success: function (paramData) {
      gTotalRecords = paramData.count;
      gTotalPages = Math.ceil(gTotalRecords / gPerPage);
      // Xóa trắng phần tử cũ
      $(".shop__content").html("");
      var vData = paramData.rows;
      if (!vData.length) {
        $("#page_container").html('<p class="my-auto">Something went wrong. Please try again later.</p>');
        return;
      }
      for (let bIndex = 0; bIndex < vData.length; bIndex++) {
        const bElement = vData[bIndex];

        // Với mỗi bản ghi sẽ tiến hành tạo một phần tử con để hiển thị
        $(".shop__content").append(`
                    <div class="col-sm-6 col-lg-3">
                      <div class="shop__item card mx-auto" style="max-width: 18rem;">
                        <img src="${bElement.imageUrl}" width="293" height="293" class="card-img-top" alt="...">
                        <div class="card-body d-flex flex-column align-items-center">
                          <h5 class="card-title item__name">${bElement.name}</h5>
                          <p class="card-text desc">${bElement.description}</p>
                          <div class="item__price">
                            <span class="new">$${bElement.promotionPrice}</span>
                            <span class="old text-decoration-line-through">$${bElement.price}</span>
                          </div>
                        </div>
                      </div>
                    </div>`);
      }
      // Hàm tạo thanh phân trang
      createPagination(vPageNum);
    },
    error: function () {
      $(".shop__content").html("error");
    },
    complete: hideSpinner
  });
}

function makeCall(vPageNum) {
  // Hàm hiển thị dữ liệu dựa vào 2 tham số phân trang
  fetch_data(vPageNum);
}

function showSpinner() {
  $('.spinnerMain').removeClass('d-none');
}

function hideSpinner() {
  $('.spinnerMain').addClass('d-none');
}
