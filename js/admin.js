"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://pucci-mart.onrender.com/api";
const gCONTENT_TYPE = "application/json;charset=UTF-8";
const gMinPrice = 0;
const gMaxPrice = 5000;
let gPetId;
let gPetTable;

// Tham số limit (Số lượng bản ghi tối đa trên 1 trang)
const gPerPage = 100;
const gQueryParams = new URLSearchParams({
  '_limit': gPerPage
});

let Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

$(document).ready(function () {
  $("#slider-range").slider({
    range: true,
    values: [0, 5000],
    min: 0,
    max: 5000,
    step: 5,
    slide: function (event, ui) {
      $("#min-price").html(ui.values[0]);

      $("#max-price").html(ui.values[1]);
    }
  });

  gPetTable = $('#pet-table').DataTable({
    searching: false,
    columns: [
      { data: 'id' },
      { data: "imageUrl" },
      { data: "name" },
      { data: "type" },
      { data: "description" },
      { data: "createdAt" },
      { data: "price" },
      { data: "promotionPrice" },
      { data: "discount" },
      { data: null },
    ],
    columnDefs: [
      {
        targets: 0,
        className: 'text-center',
        render: function (_, type, row, meta) {
          return meta.row + 1;
        }
      },
      {
        targets: 1,
        orderable: false,
        render: function (data) {
          return `<img src="${data}" class="w-100 " alt="">`;
        }
      },
      {
        targets: 4,
        orderable: false,
      },
      {
        targets: 5,
        render: function (data) {
          return data.slice(0, 10);
        }
      },
      {
        targets: 8,
        render: function (data) {
          return data ? data + '%' : '0';
        }
      },
      {
        targets: 9,
        orderable: false,
        defaultContent: `<button class="btn btn-edit"><i class="fa fa-edit"></i></button>
                            <button class="btn btn-delete text-danger "><i class="fa fa-trash-alt"></i></button>`,
      },
    ]
  });
  onPageLoading();

  $('.filterBtn').on('click', function () {
    onBtnFilterClick();
  });

  $('#btn-confirm-add').on('click', function () {
    onBtnConfirmAddClick();
  });

  $('#pet-table').on('click', '.btn-edit', function () {
    onBtnEditClick(this);
  });

  $("#btn-confirm-update").on("click", function () {
    onBtnConfirmUpdateClick();
  });

  $('#pet-table').on('click', '.btn-delete', function () {
    onBtnDeleteClick(this);
  });

  $("#btn-confirm-delete").on("click", function () {
    onBtnConfirmDeleteClick();
  });

});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onPageLoading() {
  callApiGetAllPet();
}

function callApiGetAllPet() {
  var vAPI_URL = gBASE_URL + "/pets";
  showSpinner();
  $.ajax({
    url: vAPI_URL,
    type: "GET",
    success: function (paramData) {
      loadDataToPetTable(paramData.rows);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
    complete: hideSpinner
  });
}

function callApiUpdatePet(paramObj) {
  const vAPI_URL = gBASE_URL + "/pets/";
  showSpinner();
  $.ajax({
    url: vAPI_URL + gPetId,
    type: "PUT",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramObj),
    success: function (paramResult) {
      handleUpdateSuccess(paramResult);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
      Toast.fire({
        icon: 'error',
        title: 'Please try again later!'
      });
    },
    complete: hideSpinner
  });
}

function callApiAddPet(paramDataObj) {
  const vAPI_URL = gBASE_URL + "/pets";
  showSpinner();
  $.ajax({
    url: vAPI_URL,
    type: "POST",
    contentType: gCONTENT_TYPE,
    data: JSON.stringify(paramDataObj),
    success: function () {
      handleAddSuccess();
    },
    error: function () {
      Toast.fire({
        icon: 'error',
        title: 'Please try again later!'
      });
    },
    complete: hideSpinner
  });
}

function onBtnFilterClick() {
  var vMinPrice = +$('.price-label #min-price').text();
  var vMaxPrice = +$('.price-label #max-price').text();
  if (vMinPrice == gMinPrice && vMaxPrice == gMaxPrice) {
    gQueryParams.delete('priceMin');
    gQueryParams.delete('priceMax');
    callApiGetAllPet();
  } else {
    gQueryParams.set('priceMin', vMinPrice);
    gQueryParams.set('priceMax', vMaxPrice);
    callApiFIlterPet();
  }
}

function callApiFIlterPet(){
  showSpinner();
  $.ajax({
    type: 'get',
    url: gBASE_URL + '/pets?' + gQueryParams.toString(),
    dataType: 'json',
    success: function (paramData) {
      loadDataToPetTable(paramData.rows);
    },
    error: function () {
      console.log(error);
    },
    complete: hideSpinner
  });
}

function onBtnConfirmAddClick() {
  var vAddedObj = {
    type: '',
    name: '',
    description: '',
    imageUrl: '',
    price: '',
    promotionPrice: '',
    discount: '',
  };

  getDataAddObj(vAddedObj);
  var vIsValid = validateData(vAddedObj);
  if (vIsValid) {
    callApiAddPet(vAddedObj);
  }
}

function showDataToUpdateModal(paramCourseObj) {
  $("#update-pet-modal #input-name").val(paramCourseObj.name);
  $("#update-pet-modal #input-type").val(paramCourseObj.type);
  $("#update-pet-modal #input-desc").val(paramCourseObj.description);
  $("#update-pet-modal #input-img").val(paramCourseObj.imageUrl);
  $("#update-pet-modal #input-price").val(paramCourseObj.price);
  $("#update-pet-modal #input-promotionPrice").val(paramCourseObj.promotionPrice);
  $("#update-pet-modal #input-discount").val(paramCourseObj.discount);
  $("#update-pet-modal").modal("show");
}

function onBtnConfirmUpdateClick() {
  var vAddedObj = {
    type: '',
    name: '',
    description: '',
    imageUrl: '',
    price: '',
    promotionPrice: '',
    discount: '',
  };

  getDataUpdate(vAddedObj);
  var vIsValid = validateData(vAddedObj);
  if (vIsValid) {
    callApiUpdatePet(vAddedObj);
  }
}

function onBtnEditClick(paramBtn) {
  gPetId = getPetIdFromButton(paramBtn);
  getPetById();
}

function onBtnDeleteClick(paramBtn) {
  gPetId = getPetIdFromButton(paramBtn);
  $("#delete-pet-modal").modal("show");
}

/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function getPetIdFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vRowData = gPetTable.row(vTableRow).data();
  return vRowData.id;
}

function handleAddSuccess() {
  callApiGetAllPet();
  clearAddModal();
  Toast.fire({
    icon: 'success',
    title: 'New pet was added.'
  });
}

function handleUpdateSuccess() {
  callApiGetAllPet();
  clearUpdateModal();
  Toast.fire({
    icon: 'success',
    title: 'Pet info was updated.'
  });
}

function handleDeleteSuccess() {
  callApiGetAllPet();
  $("#delete-pet-modal").modal("hide");
  Toast.fire({
    icon: 'success',
    title: 'Pet info was deleted.'
  });
}

function clearAddModal() {
  $("#input-name").val('');
  $("#input-desc").val('');
  $("#input-img").val('');
  $("#input-price").val('');
  $("#input-promotionPrice").val('');
  $("#input-discount").val('');
  $("#add-pet-modal").modal("hide");
}

function clearUpdateModal() {
  $("#update-pet-modal #input-name").val('');
  $("#update-pet-modal #input-desc").val('');
  $("#update-pet-modal #input-img").val('');
  $("#update-pet-modal #input-price").val('');
  $("#update-pet-modal #input-promotionPrice").val('');
  $("#update-pet-modal #input-discount").val('');
  $("#update-pet-modal").modal("hide");
}

function getDataAddObj(paramAddObj) {
  paramAddObj.name = $("#input-name").val().trim();
  paramAddObj.type = $("#input-type").val();
  paramAddObj.description = $("#input-desc").val().trim();
  paramAddObj.imageUrl = $("#input-img").val().trim();
  paramAddObj.price = parseInt($("#input-price").val()) || 0;
  paramAddObj.promotionPrice = parseInt($("#input-promotionPrice").val()) || 0;
  paramAddObj.discount = parseInt($("#input-discount").val()) || 0;
}

function getDataUpdate(paramObj) {
  paramObj.name = $("#update-pet-modal #input-name").val().trim();
  paramObj.type = $("#update-pet-modal #input-type").val();
  paramObj.description = $("#update-pet-modal #input-desc").val().trim();
  paramObj.imageUrl = $("#update-pet-modal #input-img").val().trim();
  paramObj.price = parseInt($("#update-pet-modal #input-price").val()) || 0;
  paramObj.promotionPrice = parseInt($("#update-pet-modal #input-promotionPrice").val()) || 0;
  paramObj.discount = parseInt($("#update-pet-modal #input-discount").val()) || 0;
}

// Call api to get Contact base on its Id
function getPetById() {
  const vAPI_URL = gBASE_URL + "/pets/";
  showSpinner();
  $.ajax({
    url: vAPI_URL + gPetId,
    type: "GET",
    success: function (paramContact) {
      showDataToUpdateModal(paramContact);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
    complete: hideSpinner
  });
}

function onBtnConfirmDeleteClick() {
  const vAPI_URL = gBASE_URL + "/pets/";
  showSpinner();
  $.ajax({
    url: vAPI_URL + gPetId,
    type: "DELETE",
    success: function () {
      handleDeleteSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
    complete: hideSpinner,
  });
}

function loadDataToPetTable(paramPets) {
  gPetTable.clear();
  gPetTable.rows.add(paramPets);
  gPetTable.draw();
}

function validateData(paramDataObj) {
  if (!paramDataObj.name) {
    alert('Please provide name');
    return false;
  }
  if (paramDataObj.description.length < 10) {
    alert('The description must contain a minimum of 10 characters');
    return false;
  }
  if (!paramDataObj.imageUrl) {
    alert('Please provide Image Url');
    return false;
  }
  if (paramDataObj.price <= 0) {
    alert('The price must be greater than 0');
    return false;
  }
  if (!paramDataObj.promotionPrice && paramDataObj.promotionPrice > paramDataObj.price) {
    alert('The promotion price must be greater than 0 and less than the original price');
    return false;
  }
  if (paramDataObj.discount < 0) {
    alert('The discount must be a number or left empty.');
    return false;
  }
  return true;
}

function showSpinner() {
  $('.spinnerMain').removeClass('d-none');
}

function hideSpinner() {
  $('.spinnerMain').addClass('d-none');
}

