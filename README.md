# Quản lý thú cưng

## 📄 Description

- ### Xem danh sách thú cưng, trang admin quản lý danh sách thú cưng.
![Full page](img/fullPage.png)
## ✨ Feature

- Trang quản lý đơn hàng:
  ![admin page](img/adminPage.png)

- Lọc theo giá bán:
  ![filter Pet](img/filterPet.png)

- Thêm Pet mới:
  ![add Pet](img/addPet.png)

- Sửa thông tin Pet:
  ![edit Pet](img/editPet.png)

- Xóa Pet khỏi danh sách quản lý:
  ![delete Pet](img/deletePet.png)


## 🧱 Technology

- Front-end:

  - Bootstrap 4
  - DataTable
  - Javascript
  - Jquery
  - Ajax
